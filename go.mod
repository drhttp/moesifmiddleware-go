module bitbucket.org/drhttp/moesifmiddleware-go

go 1.12

require (
	bitbucket.org/drhttp/moesifapi-go v1.0.3
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
)
